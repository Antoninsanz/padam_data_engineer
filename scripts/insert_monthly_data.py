import datetime
import os
import random
from typing import List, Literal, Tuple

import psycopg2
import typer
from dateutil.relativedelta import relativedelta
from psycopg2 import extensions

# The lesser, the fewer data
# Set to 1 if you want to just test your pipelines (takes 5s)
# For the test, it will be set to 100 (takes 1min)
RANDOM_MAIN_CONTROLLER = int(os.environ.get("RANDOM_MAIN_CONTROLLER", 100))


def get_connection_db(
    target: Literal["production", "warehouse"]
) -> extensions.connection:
    return psycopg2.connect(
        (
            f"postgresql://"
            f"{target}_data_user:"
            f"{target}_data_pwd@"
            f"{target}_data/"
            f"{target}_data_db"
        )
    )


def get_connection_production_db() -> extensions.connection:
    return get_connection_db(target="production")


def get_connection_warehouse_db() -> extensions.connection:
    return get_connection_db(target="warehouse")


def delete_monthly_data_entity_content(year: int, month: int):
    # Open cursor
    conn = get_connection_production_db()
    cursor = conn.cursor()

    # Drop data
    print(f"Deleting data for year {year} and month {month}")

    drop_query = f"""
    DELETE FROM "entity"
        WHERE 
            EXTRACT(YEAR FROM "created_at") = {year} 
            AND
            EXTRACT(MONTH FROM "created_at") = {month} 
    """

    cursor.execute(query=drop_query)
    conn.commit()

    # Close cursor
    cursor.close()
    conn.close()


def generate_data(year: int, month: int) -> List[Tuple[datetime.datetime, int]]:
    print(f"Generating data for year {year} and month {month}")
    random.seed(year + month)
    random_control_param = min(13 - month, month)
    random_control_scale = RANDOM_MAIN_CONTROLLER
    min_delay_between_entity_seconds = int(
        1 / random_control_param * 10_000 / (random_control_scale)
    )
    max_delay_between_entity_seconds = int(
        1 / random_control_param * 10_000 / (random_control_scale) * 2
    )
    min_value = random_control_scale * random_control_param
    max_value = 2 * random_control_scale * random_control_param

    first_date = datetime.datetime(year=year, month=month, day=1)
    current_date = first_date
    elements_to_add = []
    while current_date < first_date + relativedelta(months=1):
        if current_date.weekday() < 5:
            value = random.randint(min_value, max_value)
        else:
            value = random.randint(2 * min_value, 2 * max_value)
        elements_to_add.append((current_date, value))

        current_date += relativedelta(
            seconds=random.randint(
                min_delay_between_entity_seconds, max_delay_between_entity_seconds
            )
        )
    print(
        (
            f"Generating data for year {year} and month {month} -> "
            f"{len(elements_to_add)} elements to insert"
        )
    )
    return elements_to_add


def insert_into_entity_table(data: List[Tuple[datetime.datetime, int]]):
    # Open cursor
    conn = get_connection_production_db()
    cursor = conn.cursor()

    # Insert data
    print(f"Inserting data ({len(data)} to insert)")
    insert_query = """INSERT INTO "entity"
                (
                    "created_at",
                    "value"
                )
                VALUES (%s, %s)
    """
    cursor.executemany(query=insert_query, vars_list=data)
    conn.commit()

    # Close cursor
    cursor.close()
    conn.close()


def main(year: int, month: int):
    delete_monthly_data_entity_content(year=year, month=month)
    data = generate_data(year=year, month=month)
    insert_into_entity_table(data=data)


if __name__ == "__main__":
    typer.run(main)
