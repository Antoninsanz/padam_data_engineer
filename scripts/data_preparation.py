import psycopg2
from psycopg2 import extensions
from typing import Literal

def get_connection_db(
    target: Literal["production", "warehouse"]
) -> extensions.connection:
    return psycopg2.connect(
        (
            f"postgresql://"
            f"{target}_data_user:"
            f"{target}_data_pwd@"
            f"{target}_data/"
            f"{target}_data_db"
        )
    )

def get_connection_production_db() -> extensions.connection:
    return get_connection_db(target="production")

def get_connection_warehouse_db() -> extensions.connection:
    return get_connection_db(target="warehouse")

def create_entity_warehouse_table(cursor_warehouse):
    create_table_query = """
        CREATE TABLE IF NOT EXISTS entity_warehouse (
            entity_id integer,
            created_at timestamp,
            value integer
        )
    """
    cursor_warehouse.execute(create_table_query)

def get_max_created_at(cursor_warehouse):
    max_created_at_query = """
        SELECT COALESCE(MAX("created_at"), '1900-01-01'::timestamp) FROM entity_warehouse;
    """
    cursor_warehouse.execute(max_created_at_query)
    return cursor_warehouse.fetchone()[0]

def select_new_data(cursor_production, max_created_at):
    select_query = f"""
        SELECT * FROM "entity"
        WHERE "created_at" > %s;
    """
    cursor_production.execute(select_query, (max_created_at,))
    return cursor_production.fetchall()

def insert_new_data(cursor_warehouse, new_data):
    if new_data:
        insert_query = """
            INSERT INTO "entity_warehouse" ("entity_id", "created_at", "value")
            VALUES (%s, %s, %s)
        """
        cursor_warehouse.executemany(insert_query, new_data)

def incremental_load():
    connection_production = get_connection_production_db()
    connection_warehouse = get_connection_warehouse_db()

    try:
        cursor_production = connection_production.cursor()
        cursor_warehouse = connection_warehouse.cursor()

        create_entity_warehouse_table(cursor_warehouse)

        # Obtenir la date max de la bdd warehouse
        max_created_at = get_max_created_at(cursor_warehouse)

        # Requête pour obtenir les nouvelles données (date supérieur à la dernière date)
        new_data = select_new_data(cursor_production, max_created_at)

        # On insère si il y a des nouvelles données
        insert_new_data(cursor_warehouse, new_data)
        connection_warehouse.commit()
            
    except psycopg2.Error as e:
        print(f"Error incremental load: {e}")

    finally:
        cursor_production.close()
        cursor_warehouse.close()

if __name__ == "__main__":
    print("Data Preparation")
    incremental_load()